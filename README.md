# srt-split-for-deepl

Split an *.srt subtitle file into batches of 5000 characters to help machine-translate it with deepl.

The deepl API would be a much more suitable solution for translating long subtitle files, but it's not available in all countries. The next best thing to do is copy in batches of 5000 characters. This script uses `xclip` to make it require three key-chords per 5000 characters - paste in current batch; copy translated batch; press enter in terminal window.
