#!/usr/bin/php
<?php

error_reporting(E_ALL);

define('MAX_CHARACTERS', 2950); // reduced from previous 5000

// Split at double-NL to only split at srt boundaries
$lines = explode("\n\n", file_get_contents($_SERVER['argv'][1]));

$nextbuffer = '';
$buffwords = 0;

$nextline = 0;

global $output_buffer;
$output_buffer = '';

function flushwords($text) {

	// Counter
	static $count = 1;
	
	// Copy to clipboard
	$xclip = popen('xclip -selection clipboard', 'wb');
    $n = fwrite($xclip, $text);
    if ($n !== strlen($text)) {
		throw new \Exception("short write");
    }
    if (pclose($xclip) !== 0) {
		throw new \Exception("xclip fatal exit code");
    }
    
    // Wait for signal that a response is ready in the clipboard
    echo "Copied section #{$count} to clipboard, press enter to continue...\n";
    fgets(STDIN, 2); // wait for NL
    
    // Read from clipboard (has an extra trailing NL)
    $resp = shell_exec('xclip -out -selection clipboard');
    
    // Strip advertising lines and all trailing newlines
    $resp = str_replace("Translated with www.DeepL.com/Translator (free version)\n", "", $resp);
    $resp = trim($resp);
    
    // Add to buffer with 2x NL
    global $output_buffer;
	$output_buffer .= $resp . "\n\n";
	$count++;
}

do {
	$line = $lines[$nextline++];
	$linewords = strlen($line); // characters
	
	if ($linewords + $buffwords > MAX_CHARACTERS) {
		// flush buffer before adding
		flushwords($nextbuffer);
		$nextbuffer = '';
		$buffwords = 0;
	}
	
	$nextbuffer .= $line."\n\n";
	$buffwords += $linewords + 2;
	
	// echo "entry {$nextline} had {$linewords}, current total {$buffwords}\n";
	
} while($nextline < count($lines));

flushwords($nextbuffer);
$nextbuffer = '';
$buffwords = 0;

// All flushed
// Write the final output

file_put_contents($_SERVER['argv'][1].'.en.deepl.srt', $output_buffer);
